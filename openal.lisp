;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;
;;; openal.lisp --- OpenAL bindings for Common Lisp.
;;;
;;; Copyright (C) 2005, James Bielman  <jamesjb@jamesjb.com>
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
;;;

(in-package #:cl-openal)

(defparameter *openal-framework*
  "/System/Library/Frameworks/OpenAL.framework/OpenAL"
  "Pathname to the OpenAL framework on Mac OS X.")

(defun load-openal-library ()
  "Loads the OpenAL foreign library."
  ;; Eventually CFFI will know about Mac OS X frameworks but until
  ;; then we'll do this the hard way:
  (if (probe-file *openal-framework*)
      (load-foreign-library *openal-framework*)
      (load-foreign-library "libAL")))

(load-openal-library)

;;;# Foreign Types

(defctype ALboolean :unsigned-char)
(defctype ALbyte :char)
(defctype ALubyte :unsigned-char)
(defctype ALshort :short)
(defctype ALushort :unsigned-short)
(defctype ALint :int)
(defctype ALuint :unsigned-int)
(defctype ALfloat :float)
(defctype ALdouble :double)
(defctype ALsizei :unsigned-int)
(defctype ALenum :int)

;;;# Constants

;; ALboolean values.
(defconstant AL_FALSE 0)
(defconstant AL_TRUE 1)

;; ALenum values.
(defcenum ALenum
  (:invalid -1)
  (:none 0)
  (:source-type #x200)
  (:source-absolute #x201)
  (:source-relative #x202)
  (:cone-inner-angle #x1001)
  (:cone-outer-angle #x1002)
  (:pitch #x1003)
  (:position #x1004)
  (:direction #x1005)
  (:velocity #x1006)
  (:looping #x1007)
  (:buffer #x1009)
  (:gain #x100a)
  (:min-gain #x100d)
  (:max-gain #x100e)
  (:orientation #x100f)
  (:reference-distance #x1020)
  (:rolloff-factor #x1021)
  (:cone-outer-gain #x1022)
  (:max-distance #x1023)
  (:source-state #x1010)
  (:initial #x1011)
  (:playing #x1012)
  (:paused #x1013)
  (:stopped #x1014)
  (:buffers-queued #x1015)
  (:buffers-processed #x1016)
  (:format-mono8 #x1100)
  (:format-mono16 #x1101)
  (:format-stereo8 #x1102)
  (:format-stero16 #x1103)
  (:frequency #x2001)
  (:bits #x2002)
  (:channels #x2003)
  (:size #x2004)
  (:data #x2005)
  (:unused #x2010)
  (:pending #x2011)
  (:processed #x2012)
  (:no-error false)
  (:invalid-name #xa001)
  (:invalid-enum #xa002)
  (:invalid-value #xa003)
  (:invalid-operation #xa004)
  (:out-of-memory #xa005)
  (:vendor #xb001)
  (:version #xb002)
  (:renderer #xb003)
  (:extensions #xb004)
  (:doppler-factor #xc000)
  (:doppler-velocity #xc001)
  (:distance-model #xd000)
  (:inverse-distance #xd001)
  (:inverse-distance-clamped #xd002))

;;;# Buffer Functions

(defcfun ("alGenBuffers" %al-gen-buffers) :void
  (a ALsizei)
  (buffers :pointer))

(defun allocate-buffer ()
  "Allocate one OpenAL buffer and return its ID number.
The buffer should be freed using FREE-BUFFER when done."
  (with-foreign-object (buffer ALuint)
    (%al-gen-buffers 1 buffer)
    (mem-ref buffer 'ALuint)))

(defun allocate-buffers (count)
  "Allocate COUNT OpenAL buffers and return them in a list.
The buffers should be freed using FREE-BUFFERS when done."
  (with-foreign-object (buffers ALuint count)
    (%al-gen-buffers count buffers)
    (loop for i from 0 below count
          collect (foreign-aref buffers 'ALuint i))))

(defcfun ("alDeleteBuffers" %al-delete-buffers) :void
  (n ALsizei)
  (buffers :pointer))

(defun free-buffer (buffer)
  "Free one OpenAL buffer allocated by ALLOCATE-BUFFER."
  (with-foreign-object (buffers ALuint)
    (setf (mem-ref buffers 'ALuint) buffer)
    (%al-delete-buffers 1 buffers)))

(defun free-buffers (list-of-buffers)
  "Free each buffer in LIST-OF-BUFFERS."
  (let ((count (length list-of-buffers)))
    (with-foreign-object (buffers ALuint)
      (loop for buffer in list-of-buffers
            for index from 0
            do (setf (foreign-aref buffers 'ALuint index) buffer))
      (%al-delete-buffers count buffers))))

(defcfun ("alIsBuffer" %al-is-buffer) ALboolean
  (buffer ALuint))

(defun buffer-p (obj)
  "Return true if OBJ is a number naming an OpenAL buffer."
  (not (zerop (%al-is-buffer obj))))

(defcfun ("alBufferData" %al-buffer-data) :void
  (buffer ALuint)
  (format ALenum)
  (data :pointer)
  (size ALsizei)
  (freq ALsizei))

;; TODO: This really should not have to copy the data into a second
;; buffer on Lisps that could support sharable vectors.
(defun set-buffer-data (buffer format freq data)
  "Set the data for BUFFER with bytes from Lisp vector DATA.
DATA must be a vector allocated with MAKE-SHAREABLE-VECTOR."
  (check-type data (simple-array (unsigned-byte 8) (*)))
  (with-pointer-to-vector-data (ptr data)
    (%al-buffer-data buffer format ptr (length data) freq)))

(defcfun ("alGetBufferf" %al-get-buffer-f) :void
  (buffer ALuint)
  (pname ALenum)
  (value :pointer))

(defun get-buffer-f (buffer pname)
  "Get a float property PNAME of BUFFER and return it."
  (with-foreign-object (result :float)
    (%al-get-buffer-f buffer pname result)
    (mem-ref result :float)))

(defcfun ("alGetBufferi" %al-get-buffer-i) :void
  (buffer ALuint)
  (pname ALenum)
  (value :pointer))

(defun get-buffer-i (buffer pname)
  "Get integer property PNAME of BUFFER and return it."
  (with-foreign-object (result :int)
    (%al-get-buffer-i buffer pname result)
    (mem-ref result :int)))

;;;# Source Functions

(defcfun ("alGenSources" %al-gen-sources) :void
  (count ALsizei)
  (sources :pointer))

(defun allocate-source ()
  "Allocate one OpenAL source and return its ID number.
The source should be freed using FREE-SOURCE when done."
  (with-foreign-object (source ALuint)
    (%al-gen-sources 1 source)
    (mem-ref source 'ALuint)))

(defun allocate-sources (count)
  "Allocate COUNT OpenAL sources and return them in a list.
The sources should be freed using FREE-SOURCES when done."
  (with-foreign-object (sources ALuint count)
    (%al-gen-sources count sources)
    (loop for i from 0 below count
          collect (foreign-aref sources 'ALuint i))))

(defcfun ("alDeleteSources" %al-delete-sources) :void
  (count ALsizei)
  (sources :pointer))

(defun free-source (source)
  "Free one OpenAL source allocated by ALLOCATE-SOURCE."
  (with-foreign-object (sources ALuint)
    (setf (mem-ref sources 'ALuint) source)
    (%al-delete-sources 1 sources)))

(defun free-sources (list-of-sources)
  "Free a list of sources allocated by ALLOCATE-SOURCES."
  (let ((count (length list-of-sources)))
    (with-foreign-object (sources ALuint count)
      (loop for source in list-of-sources
            for index from 0
            do (setf (foreign-aref sources 'ALuint index) source))
      (%al-delete-sources count sources))))

(defcfun ("alIsSource" %al-is-source) ALboolean
  (source ALuint))

(defun source-p (obj)
  "Return true if OBJ names an OpenAL source."
  (not (zerop (%al-is-source obj))))

(defcfun ("alSourcei" %al-source-i) :void
  (source ALuint)
  (pname ALenum)
  (value ALint))

(defcfun ("alGetSourcei" %al-get-source-i) :void
  (source ALuint)
  (pname ALenum)
  (value :pointer))

(defun get-source-i (source pname)
  "Get integer property PNAME for SOURCE and return it."
  (with-foreign-object (result :int)
    (%al-get-source-i source pname result)
    (mem-ref result :int)))

(defcfun ("alSourcePlay" source-play) :void
  (source ALuint))

(defun wait-for-source (source)
  "Loop waiting for SOURCE to stop playing."
  (loop
     (sleep 0.01)
     (unless (eql (foreign-enum-keyword
                   'ALenum
                   (get-source-i source :source-state)) :playing)
       (return))))

(defcfun ("alSourcePause" source-pause) :void
  (source ALuint))

(defcfun ("alSourceStop" source-stop) :void
  (source ALuint))

;;;# Initializing the Library

(defcfun ("alutInit" %alut-init) :void
  (argcp :pointer)
  (argvp :pointer))

(defun alut-init ()
  "Initialize OpenAL using the alutInit utility function."
  (with-foreign-object (argcp :int)
    (setf (mem-ref argcp :int) 0)
    (%alut-init argcp (null-ptr))))

(defcfun ("alutExit" %alut-exit) :void)

(defvar *initialized-p* nil
  "If true, OpenAL has been initialized.")

(defmacro with-openal (() &body body)
  "Wrap BODY with the OpenAL library initialized."
  `(unwind-protect
        (progn
          (unless *initialized-p*
            (alut-init))
          (let ((*initialized-p* t))
            ,@body))
     (unless *initialized-p*
       (%alut-exit))))

;;;# Simple CLOS Interface

(deftype ub8 ()
  `(unsigned-byte 8))

(defclass waveform ()
  ((duration :initform 0 :initarg :duration :reader duration)
   (frequency :initform 44100 :initarg :frequency :reader frequency)
   (channels :initform 1 :initarg :channels :reader channels)
   (depth :initform 1 :initarg :depth :reader depth)
   (data :initform nil :initarg :data :accessor data))
  (:documentation "Basic object representing a sound to play."))

(defmethod size-in-bytes ((wf waveform))
  "Return the size in bytes of a waveform."
  (with-slots (frequency duration channels depth) wf
    (ceiling (* frequency duration channels depth))))

(defmethod openal-format ((wf waveform))
  "Return the OpenAL format of a WAVEFORM."
  (with-slots (channels depth) wf
    (ecase channels
      (1 (ecase depth
           (1 :format-mono8)
           (2 :format-mono16)))
      (2 (ecase depth
           (1 :format-stereo8)
           (2 :format-stereo16))))))

(defmethod silence-byte ((wf waveform))
  "Return a byte representing silence in WF."
  (ecase (depth wf)
    (1 #x80)
    (2 #x00)))

(defmethod make-data-array ((wf waveform))
  "Construct a data array of the right size for WF."
  (make-array (size-in-bytes wf) :element-type 'ub8
              :initial-element (silence-byte wf)))

(defmethod initialize-instance :after ((wf waveform) &key)
  "Allocate the data array after creating a WAVEFORM."
  (setf (data wf) (make-data-array wf)))

;;;# White Noise Waveform

(defclass white-noise (waveform)
  ()
  (:documentation "Simple waveform that contains random data."))

(defmethod initialize-instance :after ((wf white-noise) &key)
  "Generate random data for a WHITE-NOISE waveform."
  (let ((data (data wf)))
    (dotimes (i (size-in-bytes wf))
      (setf (aref data i) (random 255)))))

;;;# Square Wave Generator

(defclass square-wave (waveform)
  ((tone :initform 440 :initarg :tone :reader tone)
   (volume :initform 32 :initarg :volume :reader volume))
  (:documentation "Square wave tone generator."))

(defmethod initialize-instance :after ((wf square-wave) &key)
  "Generate square wave data for a SQUARE-WAVE instance."
  (with-slots (frequency data channels depth volume tone) wf
    (unless (= channels 1)
      (error "Stereo square waves not implemented."))
    (unless (= depth 1)
      (error "16-bit square waves not implemented."))
    (when (> volume 127)
      (error "Volume ~D out of range." volume))
    (let* ((period (floor frequency tone))
           (period/2 (floor period 2)))
      (loop for i from 0 below (size-in-bytes wf)
            if (evenp (truncate i period/2))
            do (setf (aref data i) (- 128 volume))
            else do (setf (aref data i) (+ 128 volume))))))

;;;# Simple Examples

(defmacro with-buffers (vars &body body)
  "Bind each variable in VARS to a buffer during BODY."
  `(destructuring-bind ,vars (allocate-buffers ,(length vars))
     (unwind-protect
          (progn ,@body)
       (free-buffers (list ,@vars)))))

(defmacro with-sources (vars &body body)
  "Bind each variable in SOURCES to a source during BODY."
  `(destructuring-bind ,vars (allocate-sources ,(length vars))
     (unwind-protect
          (progn ,@body)
       (free-sources (list ,@vars)))))

(defmethod play ((wf waveform))
  "Play a waveform using OpenAL.  Waits for it to finish."
  (with-slots (frequency data) wf
    (with-openal ()
      (with-buffers (buf)
        (with-sources (source)
          (set-buffer-data buf (openal-format wf) frequency data)
          (%al-source-i source :buffer buf)
          (source-play source)
          (wait-for-source source))))))
