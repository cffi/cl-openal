;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;
;;; package.lisp --- Package definition for CL-OPENAL.
;;;
;;; Copyright (C) 2005, James Bielman  <jamesjb@jamesjb.com>
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
;;;

(defpackage #:cl-openal
  (:use #:cl #:cffi)
  (:export
   ;; This API is /extremely/ preliminary..
   #:allocate-buffer
   #:allocate-buffers
   #:free-buffer
   #:free-buffers
   #:with-buffers
   #:buffer-p
   #:set-buffer-data
   #:get-buffer-f
   #:get-buffer-i
   #:allocate-source
   #:allocate-sources
   #:with-sources
   #:free-source
   #:free-sources
   #:source-p
   #:get-source-i
   #:source-play
   #:source-pause
   #:source-stop
   #:wait-for-source
   #:with-openal

   ;; The even more preliminary CLOS interface:
   #:waveform
   #:duration
   #:frequency
   #:depth
   #:channels
   #:data
   #:size-in-bytes
   #:white-noise
   #:square-wave
   #:tone
   #:volume
   #:play))
